/**
 *  TIMES LIBRARY
 *  Main Functions
 *  This file contains functions that will be exported from the library
 */

import { padNumber } from './helper-functions';

/**
 * Calculates the number of days that are in the given month 
 * @param {number} year 
 * @param {month} month
 * @return {number}
 */
export function getDaysInMonth(year = 2017, month = 0) {
    return new Date(year, month + 1, 0).getDate();
}

/**
 * Convert the day of the week value to start on monday instead of sunday
 * @param {number} day
 * @return {number}
 */
export function convertDay(day = 0) {
    if(day === 0) {
        return 6;
    } else {
        return day - 1;
    }
}

/**
 * Create an array with as values the numeric days of a month
 * @param {number} year 
 * @param {number} month
 * @return {Array<number>} 
 */
export function getDaysAsArray(year = 2017, month = 0) {
    let daysArray = [];
    
    for(let i = 1; i <= getDaysInMonth(year, month); i++) {
        daysArray.push(i);
    }

    return daysArray;
}

/**
 * Create an array from numeriv values
 * @param {number} length - The number of values in the array
 * @param {number} start - The start number of the array
 * @param {number} step - The amount of increase per step
 * @return {Array}
 */
export function createNumericArray(length = 0, start = 1) {
    let numericArray = [];

    for(let i = start; i < length; i++) {
        numericArray.push(i);
    }

    return numericArray;
}

/**
 * This section contains functions that will work with a dateString format referred to as localISO 
 * localISO is formatted as (yyyy-MM-dd) and is locale time
 */


/**
 * Get the month from a localISO string
 * @param {string} dateString 
 * @return {number}
 */
export function getLocalISOMonth(dateString) {
    return Number(dateString.slice(5, 7)) - 1;
}

/**
 * Get the year from a localISO string
 * @param {string} dateString 
 * @return {number}
 */
export function getLocalISOYear(dateString = '') {
    return Number(dateString.slice(0, 4));
}

/**
 * Get the day of the month from a localISO string
 * @param {string} dateString 
 * @return {number}
 */
export function getLocalISODay(dateString = '') {
    return Number(dateString.slice(8));
}

/**
 * Convert a javascript date object to a localISO string
 * @param {Date} date 
 * @return {string}
 */
export function toLocalISO(date = new Date()) {
    let tzo = -date.getTimezoneOffset(),
        dif = tzo >= 0 ? '+' : '-';

    return date.getFullYear() +
        '-' + padNumber(date.getMonth() + 1) +
        '-' + padNumber(date.getDate());
}

/**
 * Create a localISO string from a year month and day
 * @param {number} year 
 * @param {number} month 
 * @param {number} day 
 * @return {string}
 */
export function createLocalISO(year, month, day) {
    return year + 
        '-' + padNumber(month + 1) + 
        '-' + padNumber(day);
}

/**
 * Check if a given string is a valid localISO string
 * @param {string} dateString 
 */
export function isValidLocalISO(dateString = '') {
    if(dateString.length === 10) {
        let year = dateString.slice(0, 4), month = dateString.slice(5, 7), day = dateString.slice(8);
        if(isNaN(year)) {
            return false;
        }
        if(isNaN(month) || month < 1 || month > 12) {
            return false;
        } 
        if(isNaN(day) || day < 1 || day > getDaysInMonth(year, month)) {
            return false;
        }
        return true;
    } else {
        return false;
    }
}

/**
 * Convert a javascript Date object or a valid javascript date string format to a localISO string
 * @param {Date|string} value 
 * @return {string}
 */
export function normalizeDate(value) {
    if(Object.prototype.toString.call(value) === "[object Date]") {
        return Times.toLocalISO(value);
    } else if(typeof value === 'string') {
        if(Times.isValidLocalISO(value)) {
            return value;
        } else {
            let tmp;
            try {
                tmp = new Date(value);
                return toLocalISO(tmp);
            } catch(error) {
                return null;
            }
        }
    } else {
        this.value = null;
    }
}
