/**
 * TIMES LIBRARY
 * translations.js
 * This file provides some text translations when it comes to dealing with dates and times
 */

 /**
  * Local variables
  */
const weekdays = {
  en: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saterday', 'Sunday'],
  nl: ['Maandag', 'Dinsdag', 'Woensdag', 'Donderdag', 'Vrijdag', 'Zaterdag', 'Zondag'],
  fr: ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche']
};

const months = {
	en: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
  	nl: ['Januari', 'Februari', 'Maart', 'April', 'Mei', 'Juni', 'Juli', 'Augustus', 'September', 'Oktober', 'November', 'December'],
  	fr: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Sepembre', 'Octobre', 'Novembre', 'Decembre']
};

/**
 * Local functions
 */

/**
* getTextArray - Return an array of chosen texts in the chosen language
* 
* Expects an options object with the following options
* @param {object} options
* @param {string} options.language
* @param {boolean} options.short - Only return the first three letters of the names
* 
* @return {*array of strings | null} - Return the array of weekdays or null
*/
function getTextArray({ language = 'en', short = false, data = months } = {}) {

	if(data === null) {
		return null;
	}

	if(typeof language !== 'string') {
		return null;
	}

	// Check if the chosen language is available, choose english if it isn't available
	if(Object.keys(data).indexOf(language) === -1) {
		language = 'en'
	}

	if(short) {
		return data[language].map(value => value.slice(0, 3));
	} else {
		return data[language];
	}
 
}

/**
 * Exported functions
 */

/**
* getWeekdays
* 
* Expects an options object with the following options
* @param {object} options
* @param {string} options.language
* @param {boolean} options.short - Only return the first three letters of the names
* 
* @return {*array of strings | null} - Return the array of weekdays or null
*/
function getWeekdays({ language = 'en', short = false } = {}) {
	return getTextArray({language, short, data: weekdays});
}

/**
* getMonths
* 
* Expects an options object with the following options
* @param {*object} options
* options @param {*string} language
* options @param {*boolean} short - Only return the first three letters of the names
* 
* @return {*array of strings | null} - Return the array of months or null
*/
function getMonths({ language = 'en', short = false } = {}) {
	return getTextArray({language, short, data: months});
}

export let translations = {
	getMonths,
	getWeekdays
}
