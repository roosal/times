/**
 *  TIMES LIBRARY
 *  Helper Functions
 *  This file contains some functions to support the others and are not exported from the library
 */

/**
 * Function to add padding to a number. If it is smaller than 10 it will add a leading 0
 * 
 * @param {*number} srcNumber 
 */
export function padNumber(srcNumber = 0) {

    if(isNaN(srcNumber)) {
        return false;
    }

    let normalizedNumber = Math.floor(Math.abs(Number(srcNumber)));

    if(normalizedNumber < 10) {
        return '0' + normalizedNumber;
    } else {
        return '' + normalizedNumber;
    }
}
